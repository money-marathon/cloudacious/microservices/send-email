import json

from cloudacious import SES

emails = {
    "welcome": {
        "body_html": f"""
            <html>
                <head></head>
                <body>
                    <h1>Welcome to Lost Newbs</h1>
                    <p>Thank you for signing up!</p>
                </body>
            </html>
        """
,
        "body_text": """
            Welcome to Lost Newbs, and thank you for signing up! Check our website at lostnewbs.org :D
        """,
        "subject": "Welcome to Lost Newbs! Ready to get nerdy?",
    },
}


def main(event, context):
    """
    Sends transactional emails as requested by other microservices
    """
    print(f"{__name__}: main() event: {event}")
    
    # hardcoded values
    sender = "noreply@lostnewbs.org"
    configuration_set = "my-first-configuration-set" # yeah I know, baby steps
    
    # load sqs message
    sqs_msg = event["Records"][0]
    # event should contain:
    recipient = sqs_msg["messageAttributes"]["recipient"]["stringValue"]
    message_purpose = sqs_msg["messageAttributes"]["message_purpose"]["stringValue"]


    email = SES.email(
        recipient=recipient,
        sender=sender,
        configuration_set=configuration_set,
        template_source=template_source,
        message_purpose=message_purpose,
    )
    email.send_message()
    return